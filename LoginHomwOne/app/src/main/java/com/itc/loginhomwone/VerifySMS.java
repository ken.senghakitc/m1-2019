package com.itc.loginhomwone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewAnimator;

public class VerifySMS extends AppCompatActivity {

    private Button bsub;
    private EditText tcode;
    private String pin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_sms);

        bsub = findViewById(R.id.bSub);
        tcode = findViewById(R.id.tPinCode);
        pin = getIntent().getStringExtra("code");


    }
    // -------Method ---------
    protected void onStart(){
        super.onStart();
        bsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msgpin = tcode.getText().toString();
                if(msgpin.equals(pin)){
                    Toast.makeText(VerifySMS.this,"Success login ..",
                            Toast.LENGTH_LONG).show();

                }
                else{
                    Toast.makeText(VerifySMS.this,"Not Mach login ..",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
