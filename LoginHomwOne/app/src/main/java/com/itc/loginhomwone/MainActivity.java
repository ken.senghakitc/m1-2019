package com.itc.loginhomwone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.OneShotPreDrawListener;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> ls_phone =new ArrayList<>();
    private Button bvify ;
    private EditText tpho;
    private TextView tnew;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for(int i =0 ; i<10 ; i++){
            ls_phone.add("+855 "+i);
        }
        //--------Refer component----
        bvify = findViewById(R.id.bvf);
        tpho = findViewById(R.id.tphone);
        tnew = findViewById(R.id.tcreatNew);
    } // End onCreate --------

  @Override
    protected void onStart(){
        super.onStart();
        bvify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //----------------
                String p = tpho.getText().toString();
                if(p.equals("")){
                    Toast.makeText(MainActivity.this,"Input phone",
                            Toast.LENGTH_LONG).show();
                    tpho.requestFocus();
                    return;
                }
                else if(ls_phone.contains(p)==false){
                    Toast.makeText(MainActivity.this,"Check your phone number",
                            Toast.LENGTH_LONG).show();
                    tpho.requestFocus();
                    return;
                }else
                 {
                    try{

                        String phoneNo = "tel:xxxxxxxxxx";
                        String messageText = "SMS FROM ANDROID";
                        Msg("First");
                        int pcod = new Random().nextInt(9999);

                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(phoneNo, null,
                                messageText, null, null);




                        Intent in = new Intent(MainActivity.this,VerifySMS.class);
                        in.putExtra("code",pcod);
                        startActivity(in);
                        Toast.makeText(MainActivity.this,
                                "Please check SMS pincode",Toast.LENGTH_LONG).show();
                        finish();

                    }catch(Exception exp){
                        Toast.makeText(MainActivity.this,
                                exp.toString(),Toast.LENGTH_LONG).show();

                    }
                }
            }// end Method onClick
        });
    }// end onStart ----------



    //-------Style function call------------
    public void AddCus(View v){
        Intent in = new Intent(MainActivity.this,CreateNew.class);
        in.putExtra("lsPhone",ls_phone);
        startActivity(in);
        finish();
    }
    //------------end function Creat new Cus -------------
    // -----------MissageBox
    private void Msg(String sq){
        Toast.makeText(MainActivity.this,sq,Toast.LENGTH_LONG).show();
    }
}
